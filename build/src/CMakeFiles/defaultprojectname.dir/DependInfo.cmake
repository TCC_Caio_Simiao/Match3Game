# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/caio/Projects/unb/Match3Game/src/audio/AudioHandler.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/audio/AudioHandler.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/audio/Music.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/audio/Music.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/audio/SoundEffect.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/audio/SoundEffect.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/core/SdlInfo.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/core/SdlInfo.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/core/SdlSystems.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/core/SdlSystems.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/Animation.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/Animation.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/Camera.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/Camera.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/Game.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/Game.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/GameObject.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/GameObject.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/StateGame.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/StateGame.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/StateManager.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/StateManager.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/Timer.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/Timer.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/engine/states/game/Placeholder.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/engine/states/game/Placeholder.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/graphics/Renderer.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/graphics/Renderer.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/graphics/Sprite.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/graphics/Sprite.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/graphics/Window.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/graphics/Window.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/input/EventHandler.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/input/EventHandler.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/input/InputHandler.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/input/InputHandler.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/util/Assert.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/util/Assert.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/util/Configuration.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/util/Configuration.cpp.o"
  "/home/caio/Projects/unb/Match3Game/src/util/Logger.cpp" "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/util/Logger.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../src"
  "/usr/include/SDL2"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
