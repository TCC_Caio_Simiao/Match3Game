# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/caio/Projects/unb/Match3Game/test/test_dummy.cpp" "/home/caio/Projects/unb/Match3Game/build/test/CMakeFiles/Default_Project_Name_GTest.dir/test_dummy.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/caio/Projects/unb/Match3Game/build/src/CMakeFiles/defaultprojectname.dir/DependInfo.cmake"
  "/home/caio/Projects/unb/Match3Game/build/test/externals/gtest-1.7.0/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/caio/Projects/unb/Match3Game/build/test/externals/gtest-1.7.0/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../externals/gtest-1.7.0/include"
  "../externals/gtest-1.7.0"
  "../src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
